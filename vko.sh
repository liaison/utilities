#!/bin/sh
# Script to correlate the offending line information in WARNING kernel oops to Linux source code. 

# Global variables 
LINUX_SRC_REPO=./linux_src_git    # The git repository of Linux source code. 
LINUX_SRC_CACHE=./linux_src_cache # The cached Linux source code 

function checkout_linux_src_from_git {
    VER_STR=$1 

    # The operation on git repository should be done under the directory 
    cd $LINUX_SRC_REPO  

    ver_pat=v${VER_STR}$
    
    # To check whether there is a local branch for the specific version 
    git branch | grep $ver_pat > /dev/null  
    
    if [[ $? -eq 0 ]]; then 
        # There has been a branch already, switch to the branch 
        git checkout -f v$VER_STR     
    else 
        # Create a local branch for the specific version of Linux 
        git checkout -f -b v$VER_STR v$VER_STR

        # Something is wrong with checkout, should halt at this point. 
        # Maybe there is no such branch in the entire git repository 
        if [[ $? -ne 0 ]]; then 
            exit 1
        fi 
    fi
    
    # Jump back to the previous directory 
    cd .. 
}


function cache_linux_src {
    VER_STR=$1 

    # Create the cache directory first if it doesnot exist.
    if [[ ! -d $LINUX_SRC_CACHE ]]; then 
        mkdir $LINUX_SRC_CACHE 
    fi 

    # Retrieve and cache the Linux source code
    if [[ ! -d $LINUX_SRC_CACHE/$VER_STR  ]]; then 
        
        # Checkout the Linux source code of specific version
        checkout_linux_src_from_git $VER_STR 

        # Copy the conttent without the git metadata. 
        mkdir $LINUX_SRC_CACHE/$VER_STR 
        cp -rf $LINUX_SRC_REPO/* $LINUX_SRC_CACHE/$VER_STR 
        echo "Kept the version $VER_STR in the cache"
    fi
}


function verify {
    VER_STR=$1
    SRC_FILE=$2
    LINE_NUM=$3
    CRASH_FUNC=$4
    NEIGHBOUR_LINE=$5
    
    # Set the default query version 
    if [[ $VER_STR == "" ]]; then 
        VER_STR="2.6.32" 
    fi

    # Set the default number of neighbour lines 
    if [[ $NEIGHBOUR_LINE == "" ]]; then 
        # Set the default value to 0, i.e. show 0 lines around the offending line. 
        NEIGHBOUR_LINE=5 
    fi

    # Show the parsed arguments on the console
    echo $VER_STR $SRC_FILE $LINE_NUM $CRASH_FUNC

    # Retrieve and cache the Linux source code 
    cache_linux_src $VER_STR 

    SRC_ROOT=$LINUX_SRC_CACHE/$VER_STR 

    sed_opt=$LINE_NUM,${LINE_NUM}d 
    
    # Create another copy of the source file without  the specific line 
    sed $sed_opt $SRC_ROOT/$SRC_FILE > src_file_temp 
    
    # Display the context around the offending line. 
    # -p: show the function that is associated with the diff line, if there is any.
    # -F "WARN": show the WARN macro that is associated with the diff line, if there is any.
    diff -p $SRC_ROOT/$SRC_FILE src_file_temp > diff_output_temp
    cat diff_output_temp

    IS_CRASH_FUNC="NO"
    # Check whether the offending line falls in the right crash function 
    grep $CRASH_FUNC diff_output_temp > /dev/null 
    if [[ $? -eq 0 ]]; then 
        IS_CRASH_FUNC="YES"
    fi 


    IS_WARN_MACRO="NO"
    # Check whether the offending line is in the WARN | WARN_ON macros 
    spatch -cocci-file warn_line_range.cocci $SRC_ROOT/$SRC_FILE > cocci_output_temp 
    grep $LINE_NUM cocci_output_temp > /dev/null 
    if [[ $? -eq 0 ]]; then 
        IS_WARN_MACRO="YES"
    fi 

   
    # Print the matching results
    echo "WARN_MACRO:$IS_WARN_MACRO,CRASH_FUNC:$IS_CRASH_FUNC"
 
    # Put the result into a CSV file for further analysis 
    echo "$VER_STR,$IS_WARN_MACRO,$IS_CRASH_FUNC" >> vko.output.csv 

    # Cleanup the temporary file
    rm -f src_file_temp diff_output_temp cocci_output_temp  

}



if [[ $# -eq 1 ]]; then
    
    if [[ $1 == "-h" ]]; then
        echo "Option:"
        echo "${BASH_SOURCE[0]} [ batch_input | (VER_STR SRC_FILE LINE_NUM CRASH_FUNC) ]" 
        exit 0
    fi 

    # In case of multiple queries that are listed in an input file 
    INPUT_FILE=$1
    
    while read ver_str src_file line_num neighbour_line 
    do 
        echo "==========="    
        
        verify $ver_str $src_file $line_num $crash_func $neighbour_line
        
    done < "$INPUT_FILE" 

    exit 0

else
    # In case of a single query
    VER_STR="$1"
    SRC_FILE="$2"
    LINE_NUM="$3" 
    CRASH_FUNC=$4
    # optional input, specify the number of neighbour lines to show. 
    NEIGHBOUR_LINE="$5"   

    # Verify the case 
    verify $VER_STR $SRC_FILE $LINE_NUM $CRASH_FUNC $NEIGHBOUR_LINE 
    
    exit 0 
fi












