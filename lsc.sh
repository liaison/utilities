#!/bin/bash
# Script to download the Linux source code of specific version, and then unzip the file. 

# Global variables 
LINUX_SRC_V2_URL="https://www.kernel.org/pub/linux/kernel/v2.6"
LINUX_SRC_V3_URL="https://www.kernel.org/pub/linux/kernel/v3.x/" 


# Starting from Linux 2.6.32.28, the source code is stored in another URL. 
LINUX_LONGTERM_SRC_V2_URL="https://www.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.32/"

# URL to access the source code
ROOT_URL=""

# Input arguments 
VER_STR="$1"

# Set the default query version 
if [[ $VER_STR == "" ]]; then 
    VER_STR="2.6.32" 
fi

# Split the string separated by ., and convert the result into array with operator ()
ver_array=(`echo $VER_STR | sed -e 's/\./ /g'`)

if [[ ${ver_array[0]} == "2" ]]; then 
    ROOT_URL=$LINUX_SRC_V2_URL

    # For the subversion following 2.6.32.27 
    if [[ ${#ver_array[@]} -gt 3 && ${ver_array[2]} -eq 32  
          && ${ver_array[3]} -gt 27 ]]; then 
        ROOT_URL=$LINUX_LONGTERM_SRC_V2_URL
    fi
fi

if [[ ${ver_array[0]} == "3" ]]; then 
    ROOT_URL=$LINUX_SRC_V3_URL
fi


SRC_ARCHIVE=linux-$VER_STR.tar.gz 

# Check whether the source code has already been downloaded. 
if [[ -d linux-$VER_STR ]]; then 
    # the source folder exists, no need to download then. 
    exit 0
fi

# Download the archive of the source code 
wget --no-check-certificate $ROOT_URL/$SRC_ARCHIVE 

# Unzip the source code package 
if [[ $? -eq 0 ]]; then 
    tar xf $SRC_ARCHIVE
fi




